﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProjetoStefanini.Models;
using ProjetoStefanini.Helpers.Enums;

namespace ProjetoStefanini.Controllers
{
    [Authorize]
    public class CustumerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region INDEX
        // GET: Custumer
        public ActionResult Index(String searchString_)
        {
            //IEnumerable<ViewModelCustumer> registros = Enumerable.Empty<ViewModelCustumer>();
            //registros = GetDados(searchString_);

            if (User.IsInRole("Administrator"))
                return View(db.Custumers.ToList());
            else
                return View(db.Custumers.Where(x => x.Seller == User.Identity.Name).ToList());
        }
        #endregion

        #region DETAILS
        // GET: Custumer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Custumer custumer = db.Custumers.Find(id);
            if (custumer == null)
            {
                return HttpNotFound();
            }
            return View(custumer);
        }
        #endregion

        #region CREATE
        // GET: Custumer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Custumer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Classification,Name,Phone,Gender,City,Region,LastPurchase,Until,Seller")] Custumer custumer)
        {
            if (ModelState.IsValid)
            {
                custumer.Seller = User.Identity.Name;
                db.Custumers.Add(custumer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(custumer);
        }
        #endregion

        #region EDIT
        // GET: Custumer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Custumer custumer = db.Custumers.Find(id);
            if (custumer == null)
            {
                return HttpNotFound();
            }
            return View(custumer);
        }

        // POST: Custumer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Classification,Name,Phone,Gender,City,Region,LastPurchase,Until,Seller")] Custumer custumer)
        {
            if (ModelState.IsValid)
            {
                custumer.Seller = User.Identity.Name;
                db.Entry(custumer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(custumer);
        }
        #endregion

        #region DELETE
        // GET: Custumer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Custumer custumer = db.Custumers.Find(id);
            if (custumer == null)
            {
                return HttpNotFound();
            }
            return View(custumer);
        }

        // POST: Custumer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Custumer custumer = db.Custumers.Find(id);
            db.Custumers.Remove(custumer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region DISPOSE
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        #region IMPLEMENTAÇÃO NÃO FINALIZADA
        private IList<ViewModelCustumer> GetDados(ParametrosTelaCustumer searchModel)
        {
            db.Configuration.ProxyCreationEnabled = false;

            try
            {
                var registro = CarregaCustumer(db);
                registro = FiltrarCampos(searchModel, registro);

                return TransformerCustumerView(registro);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private IList<ViewModelCustumer> TransformerCustumerView(IQueryable<ViewModelCustumer> custumer)
        {
            var registros = new List<ViewModelCustumer>();
            if (custumer != null)
                foreach (var item in custumer)
                {
                    var registro = new ViewModelCustumer();

                    registro.Classification = item.Classification;
                    registro.City = item.City;
                    registro.Seller = item.Seller;
                    registro.Region = item.Region;
                    registro.Gender = item.Gender;
                    registro.Phone = item.Phone;
                    registro.Name = item.Name;
                    registro.LastPurchase = item.LastPurchase;
                    registro.Until = item.Until;
                    registros.Add(registro);
                }

            return registros;
        }

        private IQueryable<ViewModelCustumer> CarregaCustumer(ApplicationDbContext db)
        {
            var registro = db.Custumers
               .Select(c => new ViewModelCustumer
               {
                   Classification = c.Classification,
                   City = c.City,
                   Seller = c.Seller,
                   Region = c.Region,
                   Gender = c.Gender,
                   Phone = c.Phone,
                   Name = c.Name,
                   LastPurchase = c.LastPurchase,
                   Until = c.Until
               });

            return registro;
        }


        private IQueryable<ViewModelCustumer> FiltrarCampos(ParametrosTelaCustumer searchModel, IQueryable<ViewModelCustumer> registros)
        {
            if (searchModel.Classification != null)
            {
                registros = registros.Where(x => x.Classification == searchModel.Classification);
            }
            if (searchModel.City != null)
            {
                registros = registros.Where(x => x.City == searchModel.City);
            }
            if (!String.IsNullOrEmpty(searchModel.Name))
            {
                registros = registros.Where(c => c.Name.Contains(searchModel.Name));
            }
            if (!String.IsNullOrEmpty(searchModel.Phone))
            {
                registros = registros.Where(c => c.Phone == searchModel.Phone);
            }
            if (!String.IsNullOrEmpty(searchModel.Region))
            {
                registros = registros.Where(c => c.Region.Contains(searchModel.Region));
            }
            if (!String.IsNullOrEmpty(searchModel.Seller))
            {
                registros = registros.Where(c => c.Seller == searchModel.Seller);
            }
            return registros.AsQueryable();
        }

        #endregion
    }

    #region IMPLEMENTAÇÃO NÃO FINALIZADA
    public class ParametrosTelaCustumer
    {
        public string Classification { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public EGender Gender { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public DateTime LastPurchase { get; set; }
        public DateTime Until { get; set; }
        public string Seller { get; set; }
    }

    public class ViewModelCustumer
    {
        public string Classification { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public EGender Gender { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public DateTime LastPurchase { get; set; }
        public DateTime Until { get; set; }
        public string Seller { get; set; }
    }
    #endregion
}
