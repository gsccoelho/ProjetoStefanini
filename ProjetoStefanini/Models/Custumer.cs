﻿using ProjetoStefanini.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace ProjetoStefanini.Models
{
    public class Custumer
    {
        [Key]
        public int Id { get; set; }
        public string Classification { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public EGender Gender { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Data in invalid format!")]
        public DateTime LastPurchase { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Data in invalid format!")]
        public DateTime Until { get; set; }
        public string Seller { get; set; }

    }
}