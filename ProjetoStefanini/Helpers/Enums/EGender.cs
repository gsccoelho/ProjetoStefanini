﻿using System.ComponentModel;

namespace ProjetoStefanini.Helpers.Enums
{
    public enum EGender
    {
        [Description("Masculine")]
        M = 0,
        [Description("Female")]
        F = 1
    }
}